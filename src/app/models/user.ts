/**
 * Created by clari on 14/08/2018.
 */
export class User {
  constructor (
    public firstName: string,
    public lastName: string,
    public email: string,
    public drinkPreference: string,
    public hobbies?: string[]
  ){}
}
