import {Subject} from 'rxjs/index';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

/**
 * Created by clari on 08/08/2018.
 */
@Injectable()
export class AppareilService {

  appareilsSubject = new Subject<any[]>();

  constructor(private httpClient: HttpClient) {}

  emitAppareilSubject() {
    this.appareilsSubject.next(this.appareils.slice());
  }



  private appareils = [

  ];

  switchOnAll() {
    for (const appareil of this.appareils) {
      appareil.status = 'allumé';
    }
  }

  switchOffAll() {
    for (const appareil of this.appareils) {
      appareil.status = 'éteint';
      this.emitAppareilSubject();
    }
  }

  switchOnOne(i: number) {
    this.appareils[i].status = 'allumé';
    this.emitAppareilSubject();
  }

  switchOffOne(i: number) {
    this.appareils[i].status = 'éteint';
    this.emitAppareilSubject();
  }

  getAppareilById(id: number) {
    debugger;
    const appareil = this.appareils.find(
      element => {
        return element.id === id;
      }
      );
    return appareil;
  }

  addAppareil(name: string, status: string) {

    const appareilObject = {
      id: 0,
      name: '',
      status: ''
    };

    appareilObject.id = (this.appareils.length) + 1;
    appareilObject.name = name;
    appareilObject.status = status;
    this.appareils.push(appareilObject);
    this.emitAppareilSubject();

  }

  saveAppareilsToServer() {
    this.httpClient.put('https://projet-demo-c76b8.firebaseio.com/appareils.json', this.appareils)
      .subscribe(
        () => {console.log('Enregistrement terminé'); },
        (error) => {console.log('il y a une erreur les gars' + error); }
      );
  }

  getAppareilsFromServer() {
    this.httpClient
      .get<any[]>('https://projet-demo-c76b8.firebaseio.com/appareils.json')
      .subscribe(
        (response) => {
          this.appareils = response;
          this.emitAppareilSubject();
        },
        (error) => {
          console.log('erreur de chargement' + error);
        }
      );
  }
}
